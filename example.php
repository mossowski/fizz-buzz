<?php

require __DIR__ . '/vendor/autoload.php';

use FizzBuzz\FizzBuzz;
use FizzBuzz\FizzBuzzPrintDecorator;

echo "Basic usage - get fizzbuzz result and print it as string:\n";

$fizzbuzz = new FizzBuzz();
echo $fizzbuzz->run(2, 10);

echo "\n ----- \n";

echo "Additional usage - decorated result (fizzbuzz is written out during execution, better readable):\n";

$fizzbuzzDecorated = new FizzBuzzPrintDecorator(new FizzBuzz());
$resultDecorated = $fizzbuzzDecorated->run(5, 10);

echo "\n ----- \n";
echo "We can print out the result as string: \n";
echo $resultDecorated;

echo "\n ----- \n";

echo "But also we can use returned object for something else:\n";
var_dump($resultDecorated);
