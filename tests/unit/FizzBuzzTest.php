<?php

namespace FizzBuzzTests\Unit;

use FizzBuzz\FizzBuzz;
use FizzBuzz\FizzBuzzResult;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * Class FizzBuzzTest
 * @package FizzBuzzTests\Unit
 */
class FizzBuzzTest extends TestCase
{
    /**
     *
     */
    public function testReturnCorrectResultObject()
    {
        $fizzBuzz = new FizzBuzz();
        $this->assertInstanceOf(FizzBuzzResult::class, $fizzBuzz->run(1, 20));
    }

    /**
     *
     */
    public function testReturnResultObjectHasCorrectCount()
    {
        $fizzBuzz = new FizzBuzz();
        $this->assertEquals(22, count($fizzBuzz->run(3, 24)));
    }

    /**
     *
     */
    public function testReturnCorrectTypeString()
    {
        $fizzBuzz = new FizzBuzz(1, 20);
        $string = (string) $fizzBuzz->run(1, 20);
        $this->assertTrue(is_string($string));
    }

    /**
     *
     */
    public function testReturnCorrectPrintCorrectString()
    {
        $fizzBuzz = new FizzBuzz();
        $result = (string) $fizzBuzz->run(1, 2);
        $this->assertEquals("12", $result);
    }

    public function testReturnStringIsDivisibleBy3()
    {
        $fizzBuzz = new FizzBuzz();
        $result = (string) $fizzBuzz->run(3, 4);
        $this->assertStringStartsWith('3', $result);
        $this->assertContains(FizzBuzz::FIZZ, $result);
        $this->assertStringEndsWith('4', $result);
    }

    public function testReturnStringIsDivisibleBy5()
    {
        $fizzBuzz = new FizzBuzz();
        $result = (string) $fizzBuzz->run(10, 11);
        $this->assertStringStartsWith('10', $result);
        $this->assertContains(FizzBuzz::BUZZ, $result);
        $this->assertStringEndsWith('11', $result);
    }

    public function testReturnStringIsDivisibleBy3And5()
    {
        $fizzBuzz = new FizzBuzz();
        $result = (string) $fizzBuzz->run(15, 16);
        $this->assertStringStartsWith('15', $result);
        $this->assertContains(FizzBuzz::FIZZ.FizzBuzz::BUZZ, $result);
        $this->assertStringEndsWith('16', $result);
    }

    public function testReturnStringIsCorrectInRangeFrom1To10()
    {
        $fizzBuzz = new FizzBuzz();
        $result = (string) $fizzBuzz->run(1, 10);
        $expected = '123fizz45buzz6fizz789fizz10buzz';
        $this->assertEquals($expected, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     * @dataProvider wrongFromParameterProvider
     * @param $from
     * @param $to
     */
    public function testWrongParametersFrom($from, $to)
    {
        $fizzBuzz = new FizzBuzz();
        $fizzBuzz->run($from, $to);
    }

    /**
     * @return array
     */
    public function wrongFromParameterProvider()
    {
        return [
            [-1, 20],
            [101, 20],
            [30, 20]
        ];
    }

    /**
     * @expectedException InvalidArgumentException
     * @dataProvider wrongToParameterProvider
     * @param $from
     * @param $to
     */
    public function testWrongParametersTo($from, $to)
    {
        $fizzBuzz = new FizzBuzz();
        $fizzBuzz->run($from, $to);
    }

    /**
     * @return array
     */
    public function wrongToParameterProvider()
    {
        return [
            [1, -1],
            [20, 10],
            [30, 101]
        ];
    }

    /**
     * @expectedException InvalidArgumentException
     * @dataProvider wrongBothParameterProvider
     * @param $from
     * @param $to
     */
    public function testWrongBothParameters($from, $to)
    {
        $fizzBuzz = new FizzBuzz();
        $fizzBuzz->run($from, $to);
    }

    /**
     * @return array
     */
    public function wrongBothParameterProvider()
    {
        return [
            [-10, -1],
            [101, 102]
        ];
    }
}
