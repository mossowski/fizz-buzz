<?php

namespace FizzBuzz;

/**
 * Interface FizzBuzzInterface
 * @package FizzBuzz
 */
interface FizzBuzzInterface
{
    /**
     * @param int $from
     * @param int $to
     * @return FizzBuzzResult
     */
    public function run(int $from = 1, int $to = 100): FizzBuzzResult;
}
