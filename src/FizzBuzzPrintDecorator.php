<?php

namespace FizzBuzz;

/**
 * Class FizzBuzzPrintDecorator
 * @package FizzBuzz
 */
class FizzBuzzPrintDecorator implements FizzBuzzInterface
{
    /**
     * @var FizzBuzzInterface
     */
    private $fizzBuzz;

    /**
     * FizzBuzzPrintDecorator constructor.
     * @param FizzBuzzInterface $fizzBuzz
     */
    public function __construct(FizzBuzzInterface $fizzBuzz)
    {
        $this->fizzBuzz = $fizzBuzz;
    }

    /**
     * @param int $from
     * @param int $to
     * @return FizzBuzzResult
     */
    public function run(int $from = 1, int $to = 100): FizzBuzzResult
    {
        $result = $this->fizzBuzz->run($from, $to);

        while ($result->valid()) {
            echo "/ ". $result->key() ." / ";
            echo empty($result->current()) ? " - /" : implode(", ", $result->current())." /";
            echo "\n";
            $result->next();
        }

        $result->rewind();
        return $result;
    }
}
