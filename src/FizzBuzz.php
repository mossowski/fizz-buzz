<?php

namespace FizzBuzz;

/**
 * Class FizzBuzz
 * @package FizzBuzz
 */
class FizzBuzz implements FizzBuzzInterface
{
    const FIZZ = 'fizz';
    const BUZZ = 'buzz';

    /**
     * @param int $from
     * @param int $to
     * @return FizzBuzzResult
     */
    public function run(int $from = 1, int $to = 100) : FizzBuzzResult
    {
        $this->validate($from, $to);

        $result = new FizzBuzzResult($from);
        for ($i = $from; $i <= $to; $i++) {
            $tmp = [];
            if (($i % 3) === 0) {
                $tmp[] = self::FIZZ;
            }

            if (($i % 5) === 0) {
                $tmp[] = self::BUZZ;
            }
            //elseif (($i % 3) === 0 && ($i % 5) === 0) {
            //    $result .= self::FIZZ.self::BUZZ;
            //}
            $result->add($tmp);
        }

        $result->rewind();
        return $result;
    }

    /**
     * @param $from
     * @param $to
     */
    private function validate($from, $to)
    {
        if ($from < 1 || $from > 100 || $to < 1 || $to > 100) {
            throw new \InvalidArgumentException();
        }

        if ($from >= $to) {
            throw new \InvalidArgumentException();
        }
    }
}
