<?php

namespace FizzBuzz;

class FizzBuzzResult implements \Iterator, \Countable
{
    private $position;
    private $init;
    private $data = [];

    /**
     * FizzBuzzResult constructor.
     * @param int $init
     */
    public function __construct(int $init = 1)
    {
        $this->init = $init;
        $this->position = $init;
    }

    public function add(array $values = [])
    {
        $this->data[$this->position] = $values;
        $this->next();
    }

    /**
     * @return void
     */
    public function rewind() : void
    {
        $this->position = $this->init;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->data[$this->position];
    }

    /**
     * @return int
     */
    public function key() : int
    {
        return $this->position;
    }

    /**
     * @return void
     */
    public function next() : void
    {
        ++$this->position;
    }

    /**
     * @return bool
     */
    public function valid() : bool
    {
        return isset($this->data[$this->position]);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        $result = "";
        foreach ($this->data as $key => $value) {
            $result .= $key;
            $result .= !empty($value) ? implode("", $value) : "";
        }

        return $result;
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->data);
    }
}
