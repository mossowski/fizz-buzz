Fizz buzz example:
=================

run in terminal:

- ``composer install`` to install PHPUnit and build autoloader.
- ``php example.php`` to run some examples
- ``php ./vendor/bin/phpunit`` to run unit tests

main project files:

- The file ``src/FizzBuzz.php`` contains implementation of the task
- The file ``tests/unit/FizzBuzzTest.php`` contains unit tests for functionality 
